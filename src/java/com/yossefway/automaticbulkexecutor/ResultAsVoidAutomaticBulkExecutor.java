/***** BEGIN LICENSE BLOCK *****
 * Version: EPL 1.0
 *
 * The contents of this file are subject to the Eclipse Public
 * License Version 1.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.eclipse.org/legal/epl-v10.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Copyright (C) 2014 Yossef Yona <yossefway@gmail.com> 
 * 
 ***** END LICENSE BLOCK *****/
package com.yossefway.automaticbulkexecutor;

import java.util.Collection;
import java.util.List;

import com.yossefway.automaticbulkexecutor.AutomaticBulkExecutor.IExecutionSource;



public abstract class ResultAsVoidAutomaticBulkExecutor<PARAM_ELEM> implements IExecutionSource<PARAM_ELEM> {
    @SuppressWarnings("unchecked")
    private ResultAsListAutomaticBulkExecutor<PARAM_ELEM, List<?>> voidResultAsList = new ResultAsListAutomaticBulkExecutor<PARAM_ELEM, List<?>>(this) {

        @SuppressWarnings("rawtypes")
        @Override
        protected List<?> execute(List params,int startIndexInclusive, int endIndexExclusive) {
            ((ResultAsVoidAutomaticBulkExecutor) source).execute(params,startIndexInclusive,endIndexExclusive);
            return null;
        }

    };

    protected abstract void execute(List<PARAM_ELEM> params, int startIndexInclusive, int endIndexExclusive);

    public void launchAutomaticBuilkExecution(int bulkSize, Collection<PARAM_ELEM> longParamCollection) {
        voidResultAsList.launchAutomaticBuilkExecution(bulkSize, longParamCollection);
    }

    public void launchAutomaticBuilkExecution(int bulkSize, List<PARAM_ELEM> longParamList) {
        voidResultAsList.launchAutomaticBuilkExecution(bulkSize, longParamList);
    }

    public void setWorkInParallel(int threadNumber) {
        voidResultAsList.setWorkInParallel(threadNumber);
    }

    public void setDontWorkInParallelMode() {
        voidResultAsList.setDontWorkInParallelMode();
    }

    public void launchAutomaticBuilkExecution(List<PARAM_ELEM> longParamList) {
        voidResultAsList.launchAutomaticBuilkExecution(longParamList);
    }

    public void launchAutomaticBuilkExecution(Collection<PARAM_ELEM> longParamCollection) {
        voidResultAsList.launchAutomaticBuilkExecution(longParamCollection);
    }

    public int getBulkSize() {
        return voidResultAsList.getBulkSize();
    }

    public void setBulkSize(int bulkSize) {
        voidResultAsList.setBulkSize(bulkSize);
    }

    public void setDontExecuteForEmptyParamList(boolean dontExecuteForEmptyParamList) {
        voidResultAsList.setDontExecuteForEmptyParamList(dontExecuteForEmptyParamList);
    }

}