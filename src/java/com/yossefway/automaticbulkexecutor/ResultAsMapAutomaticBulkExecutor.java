/***** BEGIN LICENSE BLOCK *****
 * Version: EPL 1.0
 *
 * The contents of this file are subject to the Eclipse Public
 * License Version 1.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.eclipse.org/legal/epl-v10.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Copyright (C) 2014 Yossef Yona <yossefway@gmail.com> 
 * 
 ***** END LICENSE BLOCK *****/
package com.yossefway.automaticbulkexecutor;

import java.util.HashMap;
import java.util.Map;

public abstract class ResultAsMapAutomaticBulkExecutor<PARAM_ELEM, RESULT extends Map<?, ?>> extends AutomaticBulkExecutor<PARAM_ELEM, RESULT>{
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected RESULT getNewEmptyResult(){
        return (RESULT) new HashMap();
    }
}