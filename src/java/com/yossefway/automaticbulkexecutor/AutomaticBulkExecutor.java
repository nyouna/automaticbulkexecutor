/***** BEGIN LICENSE BLOCK *****
 * Version: EPL 1.0
 *
 * The contents of this file are subject to the Eclipse Public
 * License Version 1.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.eclipse.org/legal/epl-v10.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Copyright (C) 2014 Yossef Yona <yossefway@gmail.com> 
 * 
 ***** END LICENSE BLOCK *****/
package com.yossefway.automaticbulkexecutor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public abstract class AutomaticBulkExecutor<PARAM_ELEM, RESULT> {

    protected interface IExecutionSource<PARAM_ELEM> {
    }

    private int bulkSize;
    private boolean dontExecuteForEmptyParamList;
    private boolean workInParallel;
    private int threadNumber;
    protected IExecutionSource<PARAM_ELEM> source;

    public AutomaticBulkExecutor() {
        bulkSize = 500;
        dontExecuteForEmptyParamList = true;
        workInParallel=false;
        threadNumber=0;
    }

    public AutomaticBulkExecutor(IExecutionSource<PARAM_ELEM> source) {
        this();
        this.source = source;
    }

    protected abstract RESULT execute(List<PARAM_ELEM> params, int startIndexInclusive, int endIndexExclusive) throws AutomaticBuilkExecutionException;
    protected abstract RESULT getNewEmptyResult();


    public RESULT launchAutomaticBuilkExecution(int bulkSize, Collection<PARAM_ELEM> longParamCollection) {
        List<PARAM_ELEM> longParamList=null;
        if(longParamCollection!=null){
            longParamList=new ArrayList<PARAM_ELEM>(longParamCollection);
        }
        return launchAutomaticBuilkExecution(bulkSize, longParamList);

    }


    public RESULT launchAutomaticBuilkExecution(int bulkSize, List<PARAM_ELEM> longParamList) {

        ExecutorService executor=null;
        if(workInParallel){
            executor = Executors.newFixedThreadPool(threadNumber);
        }


        int entityKeyListSize=0;

        if(longParamList!=null){
            entityKeyListSize = longParamList.size();
        }
        RESULT wholeResult=null;
        boolean splitEnded=false;
        int startIndexInclusive=0;
        int endIndexExclusive;

        if(entityKeyListSize>bulkSize){
            endIndexExclusive=bulkSize;
        }
        else {
            endIndexExclusive=entityKeyListSize;
        }
        List<Future<RESULT>> futureResults = new ArrayList<Future<RESULT>>();
        while(!splitEnded){
            if(wholeResult==null){
                wholeResult=getNewEmptyResult();
            }
            if(isExecute(longParamList)){
                List<PARAM_ELEM> bulkEntityKeys = longParamList.subList(startIndexInclusive, endIndexExclusive);

                if(workInParallel){
                    ExecuteTask executorTask=new ExecuteTask(bulkEntityKeys,startIndexInclusive,endIndexExclusive);
                    futureResults.add(executor.submit(executorTask));
                }
                else {
                    RESULT bulkResult = execute(bulkEntityKeys,startIndexInclusive,endIndexExclusive);
                    addBulkResultToWholeResult(wholeResult, bulkResult);
                }

                startIndexInclusive=startIndexInclusive+bulkSize;
                if(startIndexInclusive>=entityKeyListSize){
                    splitEnded=true;
                }

                if(entityKeyListSize>endIndexExclusive+bulkSize){
                    endIndexExclusive=endIndexExclusive+bulkSize;
                }
                else {
                    endIndexExclusive=entityKeyListSize;
                }
            }
            else{
                splitEnded=true;
            }
        }
        if(workInParallel){
            collectResultsFromParallelExecution(wholeResult, futureResults);
        }
        return wholeResult;
    }

    private void collectResultsFromParallelExecution(RESULT wholeResult, List<Future<RESULT>> futureResults) {
        for (Future<RESULT> futureResult : futureResults) {
            try {
                addBulkResultToWholeResult(wholeResult, futureResult.get());
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage(),e);
            }
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected void addBulkResultToWholeResult(RESULT wholeResult, RESULT bulkResult) {
        if (bulkResult != null) {
            if (wholeResult instanceof Collection) {
                ((Collection) wholeResult).addAll((Collection) bulkResult);
            }
            else if (wholeResult instanceof Map) {
                ((Map) wholeResult).putAll((Map) bulkResult);
            }
        }
    }

    public void setWorkInParallel(int threadNumber){
        assert threadNumber>0 : "threadNumber must be positive";
        this.workInParallel=true;
        this.threadNumber=threadNumber;
    }
    public void setDontWorkInParallelMode(){
        this.workInParallel=false;
        threadNumber=0;
    }

    private class ExecuteTask implements Callable<RESULT>{

        private List<PARAM_ELEM> paramList;
        private int startIndexInclusive;
        private int endIndexExclusive;

        private ExecuteTask(List<PARAM_ELEM> paramList, int startIndexInclusive, int endIndexExclusive) {
            this.paramList = paramList;
            this.startIndexInclusive=startIndexInclusive;
            this.endIndexExclusive=endIndexExclusive;
        }
        
        @Override
        public RESULT call() throws Exception {
            return execute(paramList,startIndexInclusive,endIndexExclusive);
        }
    }



    private boolean isExecute(List<PARAM_ELEM> longParamList) {
        boolean isEmptyList=(longParamList == null || longParamList.size() == 0);
        return !(isEmptyList && dontExecuteForEmptyParamList);
    }

    public RESULT launchAutomaticBuilkExecution(List<PARAM_ELEM> longParamList) throws AutomaticBuilkExecutionException {
        return launchAutomaticBuilkExecution(bulkSize, longParamList);
    }

    public RESULT launchAutomaticBuilkExecution(Collection<PARAM_ELEM> longParamCollection) throws AutomaticBuilkExecutionException {
        return launchAutomaticBuilkExecution(bulkSize, longParamCollection);
    }
    public int getBulkSize() {
        return bulkSize;
    }
    public void setBulkSize(int bulkSize) {
        this.bulkSize = bulkSize;
    }
    public void setDontExecuteForEmptyParamList(boolean dontExecuteForEmptyParamList) {
        this.dontExecuteForEmptyParamList = dontExecuteForEmptyParamList;
    }

}