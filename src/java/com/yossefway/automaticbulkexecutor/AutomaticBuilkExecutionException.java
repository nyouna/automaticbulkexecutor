/***** BEGIN LICENSE BLOCK *****
 * Version: EPL 1.0
 *
 * The contents of this file are subject to the Eclipse Public
 * License Version 1.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.eclipse.org/legal/epl-v10.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Copyright (C) 2014 Yossef Yona <yossefway@gmail.com> 
 * 
 ***** END LICENSE BLOCK *****/
package com.yossefway.automaticbulkexecutor;

public class AutomaticBuilkExecutionException extends RuntimeException {
    private static final long serialVersionUID = 8530225305122143870L;

    public AutomaticBuilkExecutionException(Exception automaticBuilkExecutionExceptionCause) {
        super(automaticBuilkExecutionExceptionCause.getMessage(), automaticBuilkExecutionExceptionCause);
        this.automaticBuilkExecutionExceptionCause = automaticBuilkExecutionExceptionCause;
    }

    private Exception automaticBuilkExecutionExceptionCause;

    public Exception getAutomaticBuilkExecutionExceptionCause() {
        return automaticBuilkExecutionExceptionCause;
    }
}
