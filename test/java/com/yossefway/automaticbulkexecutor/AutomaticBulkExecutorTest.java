/***** BEGIN LICENSE BLOCK *****
 * Version: EPL 1.0
 *
 * The contents of this file are subject to the Eclipse Public
 * License Version 1.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.eclipse.org/legal/epl-v10.html
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Copyright (C) 2014 Yossef Yona <yossefway@gmail.com> 
 * 
 ***** END LICENSE BLOCK *****/
package com.yossefway.automaticbulkexecutor;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.yossefway.automaticbulkexecutor.AutomaticBulkExecutor;
import com.yossefway.automaticbulkexecutor.ResultAsListAutomaticBulkExecutor;
import com.yossefway.automaticbulkexecutor.ResultAsMapAutomaticBulkExecutor;
import com.yossefway.automaticbulkexecutor.ResultAsVoidAutomaticBulkExecutor;

public class AutomaticBulkExecutorTest {

    private AutomaticBulkExecutor<Integer, List<Integer>> listExecutor;
    private AutomaticBulkExecutor<Integer, Map<Integer,Integer>> mapExecutor;
    private AutomaticBulkExecutor<Integer, Map<Integer,String>> mapIntegerStringExecutor;

    private ResultAsListAutomaticBulkExecutor<Integer, List<Integer>> listExecutor2;
    private ResultAsMapAutomaticBulkExecutor<Integer, Map<Integer,Integer>> mapExecutor2;
    private ResultAsMapAutomaticBulkExecutor<Integer, Map<Integer,String>> mapIntegerStringExecutor2;

    @Before
    public void setUp() throws Exception {
        listExecutor = getIntegerListExecutor();
        mapExecutor = getIntegerIntegerMapExecutor();
        mapIntegerStringExecutor = getIntegerStringMapExecutor();

        listExecutor2= getIntegerListExecutor2();
        mapExecutor2 = getIntegerIntegerMapExecutor2();
        mapIntegerStringExecutor2 = getIntegerStringMapExecutor2();
    }


    private AutomaticBulkExecutor<Integer, Map<Integer, String>> getIntegerStringMapExecutor() {
        return new AutomaticBulkExecutor<Integer, Map<Integer,String>>() {
            @Override
            public Map<Integer,String> execute(List<Integer> params, int startIndexInclusive, int endIndexExclusive) {
                System.out.println("    execute bulk for " + params);
                if (params.size() == 0) {
                    throw new RuntimeException("empty bulk");
                }
                HashMap<Integer, String> result = new HashMap<Integer,String>();
                for (Integer integer : params) {
                    result.put(integer, "s"+integer);
                }
                return result;
            }

            @Override
            public HashMap<Integer,String> getNewEmptyResult() {
                return new HashMap<Integer,String>();
            }
        };
    }

    private AutomaticBulkExecutor<Integer, Map<Integer, Integer>> getIntegerIntegerMapExecutor() {
        return new AutomaticBulkExecutor<Integer, Map<Integer,Integer>>() {
            @Override
            public Map<Integer,Integer> execute(List<Integer> params, int startIndexInclusive, int endIndexExclusive) {
                System.out.println("    execute bulk for " + params);
                if (params.size() == 0) {
                    throw new RuntimeException("empty bulk");
                }
                HashMap<Integer, Integer> result = new HashMap<Integer,Integer>();
                for (Integer integer : params) {
                    result.put(integer, integer);
                }
                return result;
            }

            @Override
            public HashMap<Integer,Integer> getNewEmptyResult() {
                return new HashMap<Integer,Integer>();
            }
        };
    }

    private AutomaticBulkExecutor<Integer, List<Integer>> getIntegerListExecutor() {
        return new AutomaticBulkExecutor<Integer, List<Integer>>() {
            @Override
            public List<Integer> execute(List<Integer> params, int startIndexInclusive, int endIndexExclusive) {
                System.out.println("    execute bulk for "+params);
                if(params.size()==0){
                    throw new RuntimeException("empty bulk");
                }
                return params;
            }

            @Override
            public List<Integer> getNewEmptyResult() {
                return new ArrayList<Integer>();
            }

        };
    }



    private ResultAsMapAutomaticBulkExecutor<Integer, Map<Integer, String>> getIntegerStringMapExecutor2() {
        return new ResultAsMapAutomaticBulkExecutor<Integer, Map<Integer,String>>() {
            @Override
            public Map<Integer,String> execute(List<Integer> params, int startIndexInclusive, int endIndexExclusive) {
                System.out.println("    execute bulk for " + params);
                if (params.size() == 0) {
                    throw new RuntimeException("empty bulk");
                }
                HashMap<Integer, String> result = new HashMap<Integer,String>();
                for (Integer integer : params) {
                    result.put(integer, "s"+integer);
                }
                return result;
            }
        };
    }

    private ResultAsMapAutomaticBulkExecutor<Integer, Map<Integer, Integer>> getIntegerIntegerMapExecutor2() {
        return new ResultAsMapAutomaticBulkExecutor<Integer, Map<Integer,Integer>>() {
            @Override
            public Map<Integer,Integer> execute(List<Integer> params, int startIndexInclusive, int endIndexExclusive) {
                System.out.println("    execute bulk for " + params);
                if (params.size() == 0) {
                    throw new RuntimeException("empty bulk");
                }
                HashMap<Integer, Integer> result = new HashMap<Integer,Integer>();
                for (Integer integer : params) {
                    result.put(integer, integer);
                }
                return result;
            }

        };
    }

    private ResultAsListAutomaticBulkExecutor<Integer, List<Integer>> getIntegerListExecutor2() {
        return new ResultAsListAutomaticBulkExecutor<Integer, List<Integer>>() {
            @Override
            public List<Integer> execute(List<Integer> params, int startIndexInclusive, int endIndexExclusive) {
                System.out.println("    execute bulk for "+params);
                if(params.size()==0){
                    throw new RuntimeException("empty bulk");
                }
                return params;
            }

        };
    }

    @Test
    public void shouldReturnEmptyArrayForEmptyElemListIn1BulksOf5() {
        List<Integer> result = listExecutor.launchAutomaticBuilkExecution(5,
                getEmptyListParams());
        printResult(result);
        assertCorrectResultForList(new Integer[]{},result);
    }
    @Test
    public void shouldReturnNullArrayForNullIn1BulksOf5() {
        List<Integer> result = listExecutor.launchAutomaticBuilkExecution(5,
                getNullListParams());
        printResult(result);
        assertCorrectResultForList(new Integer[]{},result);
    }
    @Test
    public void shouldReturnSameArrayForListOf5ElemsIn3Bulks() {

        List<Integer> result = listExecutor.launchAutomaticBuilkExecution(2,
                getLongListParams());
        printResult(result);
        assertCorrectResultForList(new Integer[]{1,2,3,4,5},result);
    }

    @Test
    public void shouldVoidReturnSameArrayForListOf5ElemsIn3Bulks() {

        final ArrayList<Integer> result = new ArrayList<Integer>();
        ResultAsVoidAutomaticBulkExecutor<Integer> voidIntegerExecutor;
        voidIntegerExecutor = new ResultAsVoidAutomaticBulkExecutor<Integer>() {

            @Override
            protected void execute(List<Integer> params, int startIndexInclusive, int endIndexExclusive) {
                result.addAll(params);

            }
        };
        voidIntegerExecutor.launchAutomaticBuilkExecution(2,
                getLongListParams());
        assertCorrectResultForList(new Integer[] { 1, 2, 3, 4, 5 }, result);

    }

    @Test
    public void shouldReturnSameArrayForListOf5ElemsIn1BulksOf5() {
        List<Integer> result = listExecutor.launchAutomaticBuilkExecution(5,
                getLongListParams());
        printResult(result);
        assertCorrectResultForList(new Integer[]{1,2,3,4,5},result);
    }
    @Test
    public void shouldReturnSameArrayForListOf5ElemsIn1BulksOf10() {
        List<Integer> result = listExecutor.launchAutomaticBuilkExecution(10,
                getLongListParams());
        printResult(result);
        assertCorrectResultForList(new Integer[]{1,2,3,4,5},result);
    }
    @Test
    public void shouldReturnMapForListOf5ElemsIn1BulksOf2() {
        Map<Integer, Integer> result = mapExecutor.launchAutomaticBuilkExecution(2,
                getLongListParams());
        printResult(result);
        HashMap<Integer, Integer> requiredMap = getActualMapForIdentity();
        assertEquals(requiredMap, result);
    }

    @Test
    public void shouldReturnMapForListOf5ElemsIn1BulksOf5() {
        Map<Integer, Integer> result = mapExecutor.launchAutomaticBuilkExecution(5,
                getLongListParams());
        printResult(result);
        HashMap<Integer, Integer> requiredMap = getActualMapForIdentity();
        assertEquals(requiredMap, result);
    }

    @Test
    public void shouldReturnSameForListOf5ElemsIn1BulksOf10() {
        Map<Integer, Integer> result = mapExecutor.launchAutomaticBuilkExecution(10,
                getLongListParams());
        printResult(result);
        HashMap<Integer, Integer> requiredMap = getActualMapForIdentity();
        assertEquals(requiredMap, result);
    }

    @Test
    public void shouldReturnSameMapForListOf5ElemsIn1BulksOf2() {
        Map<Integer, String> result = mapIntegerStringExecutor.launchAutomaticBuilkExecution(2,
                getLongListParams());
        printResult(result);
        HashMap<Integer, String> requiredMap = getIntegerStringMap();
        assertEquals(requiredMap, result);
    }





    @Test
    public void shouldReturnSameArrayImplementation2ForListOf5ElemsIn3Bulks() {

        List<Integer> result = listExecutor2.launchAutomaticBuilkExecution(2,
                getLongListParams());
        printResult(result);
        assertCorrectResultForList(new Integer[]{1,2,3,4,5},result);
    }

    @Test
    public void shouldReturnSameArrayImplementation2ForListOf5ElemsIn1BulksOf5() {
        List<Integer> result = listExecutor2.launchAutomaticBuilkExecution(5,
                getLongListParams());
        printResult(result);
        assertCorrectResultForList(new Integer[]{1,2,3,4,5},result);
    }
    @Test
    public void shouldReturnSameArrayImplementation2ForListOf5ElemsIn1BulksOf10() {
        List<Integer> result = listExecutor2.launchAutomaticBuilkExecution(10,
                getLongListParams());
        printResult(result);
        assertCorrectResultForList(new Integer[]{1,2,3,4,5},result);
    }
    @Test
    public void shouldReturnMapImplementation2ForListOf5ElemsIn1BulksOf2() {
        Map<Integer, Integer> result = mapExecutor2.launchAutomaticBuilkExecution(2,
                getLongListParams());
        printResult(result);
        HashMap<Integer, Integer> requiredMap = getActualMapForIdentity();
        assertEquals(requiredMap, result);
    }

    @Test
    public void shouldReturnMapImplementation2ForListOf5ElemsIn1BulksOf5() {
        Map<Integer, Integer> result = mapExecutor2.launchAutomaticBuilkExecution(5,
                getLongListParams());
        printResult(result);
        HashMap<Integer, Integer> requiredMap = getActualMapForIdentity();
        assertEquals(requiredMap, result);
    }

    @Test
    public void shouldReturnSameImplementation2ForListOf5ElemsIn1BulksOf10() {
        Map<Integer, Integer> result = mapExecutor2.launchAutomaticBuilkExecution(10,
                getLongListParams());
        printResult(result);
        HashMap<Integer, Integer> requiredMap = getActualMapForIdentity();
        assertEquals(requiredMap, result);
    }

    @Test
    public void shouldReturnSameMapImplementation2ForListOf5ElemsIn1BulksOf2() {
        Map<Integer, String> result = mapIntegerStringExecutor2.launchAutomaticBuilkExecution(2,
                getLongListParams());
        printResult(result);
        HashMap<Integer, String> requiredMap = getIntegerStringMap();
        assertEquals(requiredMap, result);
    }




    private HashMap<Integer, String> getIntegerStringMap() {
        HashMap<Integer, String> actual = new HashMap<Integer,String>();
        actual.put(1, "s1");
        actual.put(2, "s2");
        actual.put(3, "s3");
        actual.put(4, "s4");
        actual.put(5, "s5");
        return actual;

    }

    private HashMap<Integer, Integer> getActualMapForIdentity() {
        HashMap<Integer, Integer> actual = new HashMap<Integer,Integer>();
        actual.put(1, 1);
        actual.put(2, 2);
        actual.put(3, 3);
        actual.put(4, 4);
        actual.put(5, 5);
        return actual;
    }

    private void printResult(Object result) {
        System.out.println("    result="+result);
    }

    private void assertCorrectResultForList(Integer[] required, List<Integer> result) {
        List<Integer> requiredList = Arrays.asList(required);
        requiredList = new ArrayList<Integer>(requiredList);
        assertEquals(requiredList, result);
    }
    private List<Integer> getEmptyListParams() {
        List<Integer> longParamList = new ArrayList<Integer>();

        System.out.println("execute for "+longParamList);
        return longParamList;
    }
    private List<Integer> getNullListParams() {
        List<Integer> longParamList = null;

        System.out.println("execute for "+longParamList);
        return longParamList;
    }
    private List<Integer> getLongListParams() {
        List<Integer> longParamList = Arrays.asList(new Integer[]{1,2,3,4,5});
        longParamList = new ArrayList<Integer>(longParamList);

        System.out.println("execute for "+longParamList);
        return longParamList;
    }
}
